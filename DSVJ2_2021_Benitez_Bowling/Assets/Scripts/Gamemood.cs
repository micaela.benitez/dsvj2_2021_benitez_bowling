﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using gameManagerScript;

namespace gamemoodScript
{ 
    public class Gamemood : MonoBehaviour
    {
        public GameObject ballActive;
        public GameObject arrowActive;
        public GameObject canvasGamemood1Active;
        public GameObject gunActive;

        private void Start()
        {
            if (GameManager.GetGamemood1())
            {
                ballActive.SetActive(true);
                arrowActive.SetActive(true);
                canvasGamemood1Active.SetActive(true);
                gunActive.SetActive(false);
            }
            else
            {
                ballActive.SetActive(false);
                arrowActive.SetActive(false);
                canvasGamemood1Active.SetActive(false);
                gunActive.SetActive(true);
            }
        }
    }
}