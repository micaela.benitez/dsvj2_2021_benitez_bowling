﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using kegelScript;
using floorScript;
using gameManagerScript;

namespace instancingKegelScript
{
    public class InstancingKegels : MonoBehaviour
    {
        public Kegel kegelPrefab;
        public List<Kegel.KegelData> kegels;
        public List<Kegel> kegelsInstantiate = new List<Kegel>();
        private float kegelsSize = 1;

        private Floor floor;
        private float maxPosX;
        private float maxPosZ;

        private void Start()
        {
            kegelPrefab.transform.localScale = Vector3.one * kegelsSize;

            floor = FindObjectOfType<Floor>();
            maxPosX = floor.transform.lossyScale.x / 2;
            maxPosZ = floor.transform.lossyScale.z / 2;

            for (int i = 0; i < kegels.Count; i++)
            {
                Kegel.KegelData kegelData = kegels[i];

                GameObject kegelsInstantiate = Instantiate(kegelPrefab, transform.position, Quaternion.identity).gameObject;

                kegelsInstantiate.transform.parent = gameObject.transform;
                kegelsInstantiate.transform.localPosition = kegels[i].pos;

                Kegel kegel = kegelsInstantiate.GetComponent<Kegel>();
                this.kegelsInstantiate.Add(kegel);
            }
        }

        private void Update()
        {
            if (GameManager.GetGamemood1())
            {
                if (Input.GetKeyDown(KeyCode.P))
                {
                    Vector3 newVec = new Vector3(UnityEngine.Random.Range(-maxPosX, maxPosX), 0, UnityEngine.Random.Range(-maxPosZ, maxPosZ));
                    GameObject kegelsInstantiate = Instantiate(kegelPrefab, newVec, Quaternion.identity).gameObject;

                    kegelsInstantiate.transform.parent = gameObject.transform;

                    Kegel kegel = kegelsInstantiate.GetComponent<Kegel>();
                    this.kegelsInstantiate.Add(kegel);
                }
            }
        }
    }
}