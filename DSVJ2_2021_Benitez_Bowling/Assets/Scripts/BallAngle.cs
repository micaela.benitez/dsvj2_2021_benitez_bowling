﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ballAngleScript
{
    public class BallAngle : MonoBehaviour
    {
        public float angleDirection = 0;
        public float angle = 0;

        void Update()
        {        
            angle += Time.deltaTime;
            angleDirection = Mathf.Sin(angle);

            transform.rotation = Quaternion.Euler(0, angleDirection * 35 + 180, 0);
        }
    }
}
