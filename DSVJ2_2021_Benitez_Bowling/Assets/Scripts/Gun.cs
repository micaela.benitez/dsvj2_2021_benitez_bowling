﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using gameManagerScript;
namespace gunScript
{
    public class Gun : MonoBehaviour
    {
        private Camera cam;
        private float kegelsSize = 1;

        public GameObject objectToInstantiate;
        public float quantity;
        public float size;
        public float upForce;

        private float rayDistance = 200;
        public LayerMask raycastLayer;
        public RaycastHit hit;

        private void Start()
        {
            cam = Camera.main;
            objectToInstantiate.transform.localScale = Vector3.one * kegelsSize;
        }

        private void Update()
        {
            float X = Input.GetAxis("Mouse X");
            float Y = Input.GetAxis("Mouse Y");

            Vector3 mousePos = Input.mousePosition;

            Ray ray = cam.ScreenPointToRay(mousePos);
            Debug.DrawRay(ray.origin, ray.direction * 50, Color.red);

            objectToInstantiate.transform.localScale = Vector3.one * size;

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit, rayDistance, raycastLayer))
                {
                    Destroy(hit.transform.gameObject);

                    for (int i = 0; i < quantity; i++)
                    {
                        GameObject kegelsInstantiate = Instantiate(objectToInstantiate, hit.point, Quaternion.identity);
                        kegelsInstantiate.GetComponent<Rigidbody>().AddRelativeForce(Vector3.up * upForce, ForceMode.Impulse);
                    }
                }
                else
                {
                    GameManager.SetWinner(false);
                    SceneManager.LoadScene("ResultScreen");
                }
            }
        }
    }
}