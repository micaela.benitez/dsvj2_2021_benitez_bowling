﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using instancingKegelScript;

namespace gameManagerScript
{
    public class GameManager : MonoBehaviour
    {
        private static int score = 0;
        private static int throws = 3;
        private static int kegels = 10;
        private static bool winner = true;
        private static bool gamemood1 = false;
        private static bool gamemood2 = false;

        private static InstancingKegels kegel;

        private static GameManager instance;
        public static GameManager Get()
        {
            return instance;
        }

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            score = 0;
            throws = 3;
            kegels = 10;
            winner = true;

            kegel = FindObjectOfType<InstancingKegels>();
        }

        public static void AddScore(int points)
        {
            score += points;
        }

        public static void ThrowsRemaining()
        {
            throws--;
        }

        public static void KegelsRemaining()
        {
            kegels = kegel.kegelsInstantiate.Count;
        }

        public static void SetWinner(bool stateWinner)
        {
            winner = stateWinner;
        }

        public static void SetGamemood1(bool stateGamemood)
        {
            gamemood1 = stateGamemood;
        }

        public static void SetGamemood2(bool stateGamemood)
        {
            gamemood2 = stateGamemood;
        }

        public static int GetScore()
        {
            return score;
        }

        public static int GetThrows()
        {
            return throws;
        }

        public static int GetKegels()
        {
            return kegels;
        }

        public static bool GetWinner()
        {
            return winner;
        }

        public static bool GetGamemood1()
        {
            return gamemood1;
        }

        public static bool GetGamemood2()
        {
            return gamemood2;
        }
    }
}