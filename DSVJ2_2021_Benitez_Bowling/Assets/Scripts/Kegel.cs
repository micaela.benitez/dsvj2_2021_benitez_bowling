﻿using System;
using System.Collections.Generic;
using UnityEngine;
using instancingKegelScript;
using gameManagerScript;

namespace kegelScript
{
    public class Kegel : MonoBehaviour
    {
        [Serializable] public class KegelData
        {
            public Vector3 pos;
        }

        public int kewelsPoints = 10;
        private MeshRenderer child;
        public InstancingKegels kegels;

        private void Start()
        {
            child = GetComponentInChildren<MeshRenderer>();
            kegels = FindObjectOfType<InstancingKegels>();
        }
        
        private void Update()
        {
            if (child.transform.localPosition.y < -1 || Vector3.Angle(child.transform.up, Vector3.up) > 80)
            {
                Destroy(gameObject);
                GameManager.AddScore(kewelsPoints);
                Kegel kegel = GetComponent<Kegel>();
                kegels.kegelsInstantiate.Remove(kegel);
            }
        }
    }
}