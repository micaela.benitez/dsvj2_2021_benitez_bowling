﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ballMotionScript;

namespace cameraScript
{
    public class CameraMotion : MonoBehaviour
    {
        private BallMotion ball;
        private float cameraInitialPos;
        private float ballInitialPos;
        private float ballActualPos;        

        private void Start()
        {
            ball = FindObjectOfType<BallMotion>();
            ballInitialPos = ball.GetComponent<Transform>().transform.position.z;

            cameraInitialPos = transform.position.z;
        }

        private void Update()
        {
            ballActualPos = ball.GetComponent<Transform>().transform.position.z;

            if (transform.position.z < 0)
            {
                if (ballActualPos > ballInitialPos)
                    transform.position = new Vector3(transform.position.x, transform.position.y, cameraInitialPos - (ballInitialPos - ballActualPos));
            }

            if (ball.transform.position.y < -1 || Input.GetKeyDown(KeyCode.R))
                transform.position = new Vector3(transform.position.x, transform.position.y, cameraInitialPos);
        }
    }
}
