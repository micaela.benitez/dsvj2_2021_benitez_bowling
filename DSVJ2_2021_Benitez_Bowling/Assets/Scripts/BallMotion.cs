﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using floorScript;
using ballAngleScript;
using gameManagerScript;

namespace ballMotionScript
{
    public class BallMotion : MonoBehaviour
    {
        public Vector3 ballInitialPos;
        public Vector3 ballActualPos;
        private float hor = 0;
        private Vector3 direction;

        private BallAngle ballAngle;
        private Floor floor;
        private Rigidbody rig;

        private float floorWidth = 0;
        private float ballRadius = 0;
        private float maxBallPos = 0;

        private float speedX = 10;
        public float force = 100;
        public int minForce = 100;
        public int maxForce = 2000;
        private int addedSpeed = 600;
        private int speedOff = 400;

        public GameObject arrowAngle;

        private void Start()
        {
            ballInitialPos = transform.position;

            ballAngle = FindObjectOfType<BallAngle>();

            floor = FindObjectOfType<Floor>();
            floorWidth = floor.transform.lossyScale.x;
            ballRadius = transform.lossyScale.x;
            maxBallPos = floorWidth / 2 - ballRadius / 2;

            rig = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            GameManager.KegelsRemaining();

            if (GameManager.GetThrows() > 0 && GameManager.GetKegels() > 0)
            {
                ballActualPos = transform.position;

                // Muevo horizontalmente la pelota
                hor = Input.GetAxis("Horizontal");

                if (ballActualPos.z == ballInitialPos.z)
                {
                    arrowAngle.SetActive(true);

                    direction = new Vector3(hor, 0, 0);
                    transform.position += direction * speedX * Time.deltaTime;

                    // Me fijo que la pelota no se pase de los limites horizontalmente            
                    if (transform.position.x > maxBallPos) transform.position = new Vector3(maxBallPos, transform.position.y, transform.position.z);
                    else if (transform.position.x < -maxBallPos) transform.position = new Vector3(-maxBallPos, transform.position.y, transform.position.z);
                }
                else
                {
                    arrowAngle.SetActive(false);
                }

                // Aumento/disminuyo la fuerza con la que se lanzara la pelota
                if (Input.GetKey(KeyCode.UpArrow)) force += addedSpeed * Time.deltaTime;
                else if (Input.GetKey(KeyCode.DownArrow)) force -= addedSpeed * Time.deltaTime;
                else force -= speedOff * Time.deltaTime;

                // Me fijo que la fuerza no sea demasiado leve ni demasiado fuerte
                if (force < minForce) force = minForce;
                else if (force > maxForce) force = maxForce;

                // Direccion con la que va a salir la pelota
                ballAngle.angle += Time.deltaTime;
                ballAngle.angleDirection = Mathf.Sin(ballAngle.angle);

                // Si aprieto space le aplico al fuerza con respecto al angulo
                if (ballActualPos.z == ballInitialPos.z)
                {
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        GameManager.ThrowsRemaining();
                        direction = new Vector3(ballAngle.angleDirection, 0, 1);
                        rig.AddForce(direction * force);
                    }
                }
            }
            else
            {
                arrowAngle.SetActive(false);
                if (GameManager.GetThrows() == 0 && GameManager.GetKegels() > 0) 
                    GameManager.SetWinner(false);
            }

            if (ballActualPos.y < -5 || Input.GetKeyDown(KeyCode.R))
            {
                rig.Sleep();
                transform.position = ballInitialPos;
                transform.rotation = new Quaternion(0, 0, 0, 0);
                force = 0;                
            }            
        }
    }
}