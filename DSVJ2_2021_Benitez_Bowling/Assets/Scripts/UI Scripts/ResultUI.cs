﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using gameManagerScript;

namespace resultUIScript
{
    public class ResultUI : MonoBehaviour
    {
        public TMP_Text resultText;
        public TMP_Text kegelsText;
        public TMP_Text throwsText;
        public TMP_Text scoreText;

        private void Update()
        {
            if (GameManager.GetWinner()) resultText.text = "Winner";
            else resultText.text = "Loser";

            if (GameManager.GetGamemood1())
            {
                scoreText.text = "Score: " + GameManager.GetScore();
                throwsText.text = "Remaining throws: " + GameManager.GetThrows();
                kegelsText.text = "Remaining kegels: " + GameManager.GetKegels();
            }               
        }

        public void LoadScene()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}