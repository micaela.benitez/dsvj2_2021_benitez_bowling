﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using gameManagerScript;

namespace menuUIScript
{
    public class MenuUI : MonoBehaviour
    {
        public void LoadGamemood1()
        {
            SceneManager.LoadScene("Bowling");
            GameManager.SetGamemood1(true);
            GameManager.SetGamemood2(false);
        }

        public void LoadGamemood2()
        {
            SceneManager.LoadScene("Bowling");
            GameManager.SetGamemood1(false);
            GameManager.SetGamemood2(true);
        }

        public void LoadCredits()
        {
            SceneManager.LoadScene("Credits");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}
