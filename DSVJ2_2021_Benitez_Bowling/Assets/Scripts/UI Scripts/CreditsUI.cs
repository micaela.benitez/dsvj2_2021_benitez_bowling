﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace creditsUIScript
{
    public class CreditsUI : MonoBehaviour
    {
        public void LoadScene()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
