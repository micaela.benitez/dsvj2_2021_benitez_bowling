﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace gameCanvasUIScript
{
    public class GameCanvasUI : MonoBehaviour
    {
        public void BackToMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
