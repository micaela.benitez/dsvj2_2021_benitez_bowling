﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using ballMotionScript;
using gameManagerScript;

namespace gameCanvasGamood1UIScript
{
    public class GameCanvasGamood1UI : MonoBehaviour
    {
        public TMP_Text kegelsText;
        public TMP_Text throwsText;
        public TMP_Text pointsText;

        private BallMotion ball;

        private void Start()
        {
            ball = FindObjectOfType<BallMotion>();
        }

        private void Update()
        {
            kegelsText.text = "Kegels: " + GameManager.GetKegels();
            throwsText.text = "Throws: " + GameManager.GetThrows();
            pointsText.text = "Points: " + GameManager.GetScore();

            if ((GameManager.GetThrows() == 0 && (ball.transform.position.y < -1 || Input.GetKeyDown(KeyCode.R))) ||
                  GameManager.GetKegels() == 0)
                SceneManager.LoadScene("ResultScreen");
        }
    }
}
