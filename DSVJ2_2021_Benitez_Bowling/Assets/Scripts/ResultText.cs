﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using gameManagerScript;

namespace resultTextScript
{
    public class ResultText : MonoBehaviour
    {
        public GameObject canvasGamemood1;
        public GameObject canvasGamemood2;

        private void Start()
        {
            if (GameManager.GetGamemood1())
            {
                canvasGamemood1.SetActive(true);
            }
            else
            {
                canvasGamemood2.SetActive(true);
            }
        }
    }
}