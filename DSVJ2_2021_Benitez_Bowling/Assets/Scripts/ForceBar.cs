﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using ballMotionScript;

public class ForceBar : MonoBehaviour
{
    public float force;
    private BallMotion ball;
    public Image forceBar;    

    void Start()
    {
        ball = FindObjectOfType<BallMotion>();
    }

    void Update()
    {
        force = ball.force;
        force = Mathf.Clamp(force, ball.minForce, ball.maxForce);
        forceBar.fillAmount = force / ball.maxForce;
    }
}
